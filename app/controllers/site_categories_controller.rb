#Generado con Keppler.
class SiteCategoriesController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_site_category, only: [:show, :edit, :update, :destroy]

  # GET /site_categories
  def index
    site_categories = SiteCategory.searching(@query).all
    @objects, @total = site_categories.page(@current_page), site_categories.size
    redirect_to site_categories_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /site_categories/1
  def show
  end

  # GET /site_categories/new
  def new
    @site_category = SiteCategory.new
  end

  # GET /site_categories/1/edit
  def edit
  end

  # POST /site_categories
  def create
    @site_category = SiteCategory.new(site_category_params)

    if @site_category.save
      redirect_to @site_category, notice: 'La categoría fue creada satisfactoriamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /site_categories/1
  def update
    if @site_category.update(site_category_params)
      redirect_to @site_category, notice: 'La categoría fue actualizada satisfactoriamente.'
    else
      render :edit
    end
  end

  # DELETE /site_categories/1
  def destroy
    @site_category.destroy
    redirect_to site_categories_url, notice: 'La categoría fue eliminada satisfactoriamente.'
  end

  def destroy_multiple
    SiteCategory.destroy redefine_ids(params[:multiple_ids])
    redirect_to site_categories_path(page: @current_page, search: @query), notice: "Categorías eliminadas satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site_category
      @site_category = SiteCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def site_category_params
      params.require(:site_category).permit(:name, :description, :icon_white, :icon_color)
    end
end
