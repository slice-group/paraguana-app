#Generado con Keppler.
class SitesController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_site, only: [:show, :edit, :update, :destroy]
  before_action :set_categories, only: [:new, :edit, :update, :create]

  # GET /sites
  def index
    sites = Site.searching(@query).all
    @objects, @total = sites.page(@current_page), sites.size
    redirect_to sites_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /sites/1
  def show
  end

  # GET /sites/new
  def new
    @site = Site.new
  end

  # GET /sites/1/edit
  def edit
  end

  # POST /sites
  def create
    @site = Site.new(site_params)

    if @site.save
      redirect_to @site, notice: 'El sitio fue creado satisfactoriamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /sites/1
  def update
    if @site.update(site_params)
      redirect_to @site, notice: 'El sitio fue actualizado satisfactoriamente.'
    else
      render :edit
    end
  end

  # DELETE /sites/1
  def destroy
    @site.get_likes.delete_all
    @site.get_upsaves.delete_all
    @site.comments.delete_all
    @site.destroy
    redirect_to sites_url, notice: 'El sitio fue eliminado satisfactoriamente.'
  end

  def destroy_multiple
    Site.destroy redefine_ids(params[:multiple_ids])
    redirect_to sites_path(page: @current_page, search: @query),
                            notice: 'Sitios eliminados satisfactoriamente'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site
      @site = Site.find(params[:id])
    end


    def set_categories
      @categories = SiteCategory.order(:name)
    end

    # Only allow a trusted parameter "white list" through.
    def site_params
      params.require(:site).permit(:name, :img, :site_category_id,
                                   :address, :description, :facebook,
                                   :twitter, :web, :latitude, :longitude
                                  )
    end
end
