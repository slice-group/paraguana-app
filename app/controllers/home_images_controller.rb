#Generado con Keppler.
class HomeImagesController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_home_image, only: [:show, :edit, :update, :destroy]

  # GET /home_images
  def index
    home_images = HomeImage.searching(@query).all
    @objects, @total = home_images.page(@current_page), home_images.size
    redirect_to home_images_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /home_images/1
  def show
  end

  # GET /home_images/new
  def new
    @home_image = HomeImage.new
  end

  # GET /home_images/1/edit
  def edit
  end

  # POST /home_images
  def create
    @home_image = HomeImage.new(home_image_params)

    if @home_image.save
      redirect_to @home_image, notice: 'La imagen fue creada satisfactoriamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /home_images/1
  def update
    if @home_image.update(home_image_params)
      redirect_to @home_image, notice: 'La imagen fue actualizada satisfactoriamente.'
    else
      render :edit
    end
  end

  # DELETE /home_images/1
  def destroy
    @home_image.destroy
    redirect_to home_images_url, notice: 'La imagen fue eliminada satisfactoriamente.'
  end

  def destroy_multiple
    HomeImage.destroy redefine_ids(params[:multiple_ids])
    redirect_to home_images_path(page: @current_page, search: @query), notice: "Imagenes eliminadas satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_home_image
      @home_image = HomeImage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def home_image_params
      params.require(:home_image).permit(:img)
    end
end
