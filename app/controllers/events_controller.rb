#Generado con Keppler.
class EventsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :set_site

  # GET /events
  def index
    events = Event.where(site_id: @site.id).searching(@query).all
    @objects, @total = events.page(@current_page), events.size
    redirect_to events_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /events/1
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  def create
    @event = Event.new(event_params)
    @event.site_id = set_site.id
    if @event.save
      redirect_to site_event_path(@event, site_id: @site.id), notice: 'El evento fue creado satisfactoriamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      redirect_to site_event_path(@event, site_id: @site.id), notice: 'El evento fue actualizado satisfactoriamente.'
    else
      render :edit
    end
  end

  # DELETE /events/1
  def destroy
    @event.destroy
    redirect_to site_events_path, notice: 'El evento fue eliminado satisfactoriamente.'
  end

  def destroy_multiple
    Event.destroy redefine_ids(params[:multiple_ids])
    redirect_to site_events_path(page: @current_page, search: @query), notice: "Eventos eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def set_site
      @site = Site.find(params[:site_id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(:name, :address, :img, :description, :web, :site_id)
    end
end
