module Api
  module V1
    # SocialsController
    class SocialsController < ApiController
      before_action :set_site

      def likes
        if user_with_token.voted_up_on? @site
          @site.disliked_by user_with_token
          render json: {
                        message: 'false',
                        token: user_with_token.authentication_token },
                 status: 200
        else
          @site.liked_by user_with_token
          render json: {
                        message: 'true',
                        token: user_with_token.authentication_token },
                 status: 200
        end
      end

      def checks
        if user_with_token.saved_up_on? @site
          @site.downsave_from user_with_token
          render json: {
                        message: 'false',
                        token: user_with_token.authentication_token },
                 status: 200
        else
          @site.upsaved_by user_with_token
          render json: {
                        message: 'true',
                        token: user_with_token.authentication_token },
                 status: 200
        end
      end

      private

      def set_site
        @site = Site.find(params[:site_id])
      end
    end
  end
end
