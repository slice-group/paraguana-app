module Api
  module V1
    # SessionsController Api
    class SessionsController < Devise::SessionsController
      protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' }
      prepend_before_filter :require_no_authentication, :only => [:create]
      before_filter :validate_auth_token, :except => :create
      before_filter :create_password, only: :create
      before_filter :set_rol, only: :create
      include Devise::Controllers::Helpers
      include ApiHelper
      respond_to :json

      # method => POST
      # uri => /api/v1/users/sign_in
      # params => { "user": { "email": "", "url_img": "", "url_profile": "", "name": "" } }
      def create
        resource = User.find_for_database_authentication(email: session_params[:email])
        unless resource
          resource = User.new(session_params)
          unless resource.save(validate: false)
            format.json { render json: resource.errors, status: :unprocessable_entity }
          end
          resource
        end
        sign_in(:user, resource)
        resource.ensure_authentication_token!
        render json: { user_id: resource.id, message: 'Logged In',
                       success: 'true', token: resource.authentication_token }
      end

      def destroy
        resource.restore_authentication_token!
        Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
        render status: 200, json: { success: 'true',
                                    message: 'Ha salido del sistema' }
      end

      def failure
        return render json: { success: false,
                              errors: [t('api.v1.sessions.invalid_login')] },
                      status: :unauthorized
      end

      private

      def session_params
        params.require(:user).permit(:name, :email, :url_img,
                                     :url_profile, :role_ids,
                                     :encrypted_password,
                                     :description, :facebook)
      end

      def create_password
        params[:user][:encrypted_password].nil? ? params[:user][:encrypted_password] = "0" : params[:user][:encrypted_password]
      end

      def set_rol
        params[:user][:role_ids].nil? ? params[:user][:role_ids] = 2 : params[:user][:role_ids]
      end
    end
  end
end
