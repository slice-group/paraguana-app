module Api
  module V1
    # EventsController
    class EventsController < ApiController

      def index
        events = Event.where(site_id: params[:site_id])
        unless events.empty?
          render json: { event: events_json(events),
                                 token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'No existen eventos en el sitio solicitado.' },
                 status: 200
        end
      end

      def show
        events = Event.where(site_id: params[:site_id], id: params[:id])
        unless events.empty?
          render json: { event: events_json(events),
                                 token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'No existe el Evento solicitado.',
                                 token: user_with_token.authentication_token },
                 status: 200
        end
      end

      private

      def events_json(events)
        events.as_json(
          only: [:id, :name, :description,
                 :address, :web, :site_id],
          methods: [:image]
        )
      end
    end
  end
end