module Api
  module V1
    # CommentsController
    class CommentsController < ApiController
      before_action :set_site

      def create
        comment = @site.comments.new(comment_params)

        if Obscenity.profane?(comment.comment)
          render json: {
            message: 'No publicaremos tu comentario por vocabulario obsceno.',
            success: 'true' },
                 status: 200
        else
          save_comment!(comment)
        end
      end

      def destroy
        comment = @site.comments.find(params[:id])

        if (comment.user_id == user_with_token.id) ||
           (user_with_token.has_role? :admin)
          destroy_comment!(comment)
        else
          render json: {
            message: 'No está autorizado para eliminar este comentario.',
            success: 'false'  },
                 status: 200
        end
      end

      private

      def save_comment!(comment)
        if comment.save
          render json: { params: comment_params, success: 'true',
                         token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'Error', success: 'false' },
                 status: 200
        end
      end

      def destroy_comment!(comment)
        if comment.get_likes.delete_all && comment.delete
          render json: { message: 'El comentario ha sido eliminado',
                         success: 'true',
                         token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'Error', success: 'false' }, status: 200
        end
      end

      def set_site
        @site = Site.find(params[:site_id])
      end

      def comment_params
        params.require(:comment).permit(:comment)
              .merge(commentable_id: params[:site_id], user_id: user_with_token.id)
      end
    end
  end
end
