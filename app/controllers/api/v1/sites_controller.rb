module Api
  module V1
    # SitesController
    class SitesController < ApiController

      def search
        categories = params[:categories].split(',')
                                        .map! { |a| SiteCategory.find_by_name(a) }
                                        .compact.map { |a| a.id }

        sites = Site.api_searching(params[:query], categories).all

        if sites.count.zero?
          render json: { message: 'No se encontraron resultados de búsqueda.' },
                 status: 200
        else
          render json: { sites: search_sites_json(sites),
                         token: user_with_token.authentication_token },
                 status: 200
        end
      end

      def site_profile
        site = Site.find(params[:id])
        if site
          render json: { site: site_profile_json(site),
                         token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'No existe el Sitio solicitado.' },
                 status: 200
        end
      end

      private

      def search_sites_json(objects)
        objects.as_json(
          only: [:id, :name, :description,
                 :address, :facebook,
                 :twitter, :web],
          methods: [:category, :color_icon, :image, :count_likes, :count_checks]
        )
      end

      def site_profile_json(site)
        site.as_json(
          only: [:id, :name, :description, :address, :latitude,
                 :longitude, :facebook, :twitter, :web],
          methods: [:category,:white_icon, :image, :all_comments, :all_likes, :all_checks, :all_events]
        )
      end
    end
  end
end
