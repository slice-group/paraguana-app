module Api
  module V1
    # CategoriesController
    class CategoriesController < ApplicationController

      def index
        categories = SiteCategory.all.order(:name)
        home_image = HomeImage.all.sample.image
        if categories.any?
          render json: { image: home_image,
                         categories: categories.as_json(
                                      only: [:id, :name],
                                      methods: [:white_icon, :color_icon]
                                     ),
                         message: true
                        }
        else
          render json: { categories: 'No se encontraron categorías.',
                         message: false }
        end
      end
    end
  end
end
