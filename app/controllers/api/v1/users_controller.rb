module Api
  module V1
    # UsersController
    class UsersController < ApiController
      def user_profile
        user = User.find(params[:id])
        if user
          render json: { user: user_profile_json(user),
                         token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'No existe el Usuario solicitado.' },
                 status: 200
        end
      end

      def edit_profile
        user = User.find(params[:id])
        user.description = user_params[:description]
        user.facebook = user_params[:facebook]
        if user.save(validate: false)
          render json: { user: user_profile_json(user),
                         token: user_with_token.authentication_token },
                 status: 200
        else
          render json: { message: 'Error' },
                 status: 401
        end
      end

      private

      def user_profile_json(user)
        user.as_json(
          only: [:id, :name, :url_img, :description, :facebook],
          methods: [:my_sites]
        )
      end

      def user_params
        params.require(:user).permit(:name, :email,
                                     :url_profile, :url_img,
                                     :description, :facebook
                                    )
      end
    end
  end
end
