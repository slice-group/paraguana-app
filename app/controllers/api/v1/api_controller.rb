module Api
  module V1
    # ApiController
    class ApiController < ApplicationController
      skip_before_filter :verify_authenticity_token
      before_action :validate_authentication_token

      def user_with_token
        User.find_by_authentication_token(
          request.headers['X-User-Token']
        )
      end

      private

      def validate_authentication_token
        if token.nil?
          return render json: {
            success: false,
            message: 'Debes autenticarte para poder tener acceso'
          }, status: 200
        else
          token.save
        end
      end

      def token
        User.find_by_authentication_token(
          request.headers['X-User-Token']
        )
      end
    end
  end
end
