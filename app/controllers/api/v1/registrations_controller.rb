module Api
  module V1
    # RegistrationsController Api
    class RegistrationsController < Devise::RegistrationsController
      include ApiHelper
      protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' }
      skip_before_filter :authenticate_scope!, :only => [:update]
      before_filter :validate_auth_token, :except => :create
      respond_to :json

      def update
        prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
        logger.debug(params[:user])
        if resource.update_with_password(account_update_params)
          if is_navigational_format?
            update_needs_confirmation?(resource, prev_unconfirmed_email)
          end
          sign_in resource_name, resource
          return render json: { success: true }
        else
          return render status: 401, json: { errors: resource.errors }
        end
      end

      def account_update_params
        params.require(:user).permit(:description, :facebook)
      end
    end
  end
end
