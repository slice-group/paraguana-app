# Generado por keppler
require 'elasticsearch/model'
class Event < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  mount_uploader :img, ImageUploader
  validates_presence_of :name, :description, :address, :img
  validates_uniqueness_of :name
  belongs_to :site

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      search(self.query query).records.order(id: :desc)
    else
      order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query,
                               fields: [:name,
                                        :address,
                                        :description
                                       ],
                               operator: :and
                             }
             }, sort: { id: 'desc' }, size: count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: id.to_s,
      name:  name.to_s,
      address:  address.to_s,
      img:  img.to_s,
      description:  description.to_s,
      web:  web.to_s,
      site_id:  site_id.to_s,
    }.as_json
  end

  def image
    img.url
  end
end
#Event.import
