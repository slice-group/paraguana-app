require 'elasticsearch/model'
# Site
class Site < ActiveRecord::Base
  mount_uploader :img, ImageUploader
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :site_category
  validates_uniqueness_of :name
  validates_presence_of :name,
                        :address,
                        :description,
                        :latitude,
                        :longitude,
                        :site_category_id
  acts_as_commentable
  acts_as_votable
  acts_as_saveable
  has_many :events, dependent: :destroy

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.clean_site!
    all.map do |a|
      a.comments.delete_all
      a.votes_for.delete_all
      a.saves_for.delete_all
    end
  end

  def self.reload_index!
    __elasticsearch__.delete_index!
    __elasticsearch__.create_index!
    __elasticsearch__.import
  end

  settings analysis: {
    analyzer: {
      spanish_snowball: {
        type: 'snowball',
        language: 'Spanish',
        filter: %w(asciifolding lowercase)
      },
      autocomplete: {
        type:      'custom',
        tokenizer: 'standard',
        filter: %w(lowercase autocomplete_filter)
      }
    },
    filter: {
      autocomplete_filter: {
        type: 'edge_ngram',
        min_gram: 1,
        max_gram: 20
      }
    }
  } do
    mapping do
      [:id, :name, :category,
       :site_category_id, :address,
       :description, :category_description].each do |attribute|
        indexes attribute, type: 'string', analyzer: 'spanish_snowball'
      end
    end
  end

  def self.searching(query)
    if query
      search(self.query query).records.order(name: :asc)
    else
      order(name: :asc)
    end
  end

  def self.api_searching(query, categories)
    if query.empty? && categories.empty?
      all.order(name: :asc)
    elsif query.empty? || categories.empty?
      search(Site.query_filter_or(query, categories)).records.order(name: :asc)
    else
      search(Site.query_filter_and(query, categories)).records.order(name: :asc)
    end
  end

  def self.query(query)
    { query: { match: { _all: { query: query, fuzziness: 'auto', operator: :and } }
              }, sort: { name: 'asc' }, size: count }
  end

  def self.query_filter_or(query, array)
    {
      query: {
        bool: {
          should: [{
            match:  {
              _all: {
                query: query,
                fuzziness: 'auto'
              }
            }
          }, terms: { site_category_id: array }]
        }
      }, sort: { name: 'asc' }, size: count
    }
  end

  def self.query_filter_and(query, array)
    {
      query: {
        filtered: {
          query: {
            match:  {
              _all: {
                query: query,
                fuzziness: 'auto'
              }
            }
          }, filter: { terms: { site_category_id: array } }
        }
      }, sort: { name: 'asc' }, size: count
    }
  end

  # Armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: id.to_s,
      name: name,
      category: site_category.name,
      site_category_id: site_category.id,
      address: address,
      description: description,
      category_description: site_category.description
    }.as_json
  end

  def category
    site_category.name
  end

  def color_icon
    site_category.icon_color.url
  end

  def white_icon
    site_category.icon_white.url
  end

  def image
    img.url
  end

  def likes
    get_likes.as_json(only: [:id, :voter_id], methods: [:user_name])
  end

  def checks
    get_upsaves.as_json(only: [:id, :voter_id], methods: [:user_name])
  end

  def count_likes
    get_upvotes.size
  end

  def count_checks
    get_upsaves.size
  end

  def all_likes
    get_upvotes.as_json(only: [:id],
                        include: [{ voter: { only: [:id, :name] } }])
  end

  def all_checks
    get_upsaves.as_json(only: [:id],
                        include: [{ saver: { only: [:id, :name] } }])
  end

  def all_comments
    comments.as_json(only: [:id, :comment],
                     methods: [:likes],
                     include: [{ user: { only: [:id, :name, :url_img] }
                               }]
                    )
  end

  def all_events
    events.as_json(
      only: [:id, :name, :description, :web, :site_id],
      methods: [:image]
    )
  end
end

# Site.import
