#Generado por keppler
require 'elasticsearch/model'
class HomeImage < ActiveRecord::Base
  mount_uploader :img, ImageUploader
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  validates_presence_of :img

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      img:  self.img.to_s,
    }.as_json
  end

  def image
    img.url
  end

end
#HomeImage.import
