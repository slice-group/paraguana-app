#Generado por keppler
require 'elasticsearch/model'
class SiteCategory < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  mount_uploader :icon_white, IconUploader
  mount_uploader :icon_color, IconUploader
  validates_presence_of :name, :description, :icon_color, :icon_white
  validates_uniqueness_of :name
  has_many :sites, dependent: :destroy

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(name: :asc)
    else
      self.order(name: :asc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { name: "asc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name.to_s,
      description:  self.description.to_s,
    }.as_json
  end

  def white_icon
    icon_white.url
  end

  def color_icon
    icon_color.url
  end

end
#SiteCategory.import
