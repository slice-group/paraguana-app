# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

[:admin, :client].each do |name|
  Role.create name: name
  puts "#{name} creado"
end

User.create name: "Admin", email: "admin@inyxtech.com", role_ids: "1", password: "12345678", password_confirmation: "12345678"
puts "admin@inyxtech.com ha sido creado"

#
#SiteCategory.create(
#  name: 'Alojamiento',
#  description: 'Hoteles, posadas, moteles, campings y apartahoteles.',
#  icon_white: Faker::Avatar.image,
#  icon_color: Faker::Avatar.image
#)
#puts 'Alojamientos created'

#SiteCategory.create(
#  name: 'Costas',
#  description: 'Playas, ensenadas, balnearios, acantilados, arrecifes, cayos, barras, islas, fiordos, canales, penínsulas, bahías y caletas.',
#  icon_white: Faker::Avatar.image,
#  icon_color: Faker::Avatar.image
#)
#puts 'Costas created'

#SiteCategory.create(
#  name: 'Salud',
#  description: 'Hospitales, clínicas, centros asistenciales, ambulatorios, consultorios médicos y farmacias..',
#  icon_white: Faker::Avatar.image,
#  icon_color: Faker::Avatar.image
#)
#puts 'Salud created'

#SiteCategory.create(
  #name: 'Transporte',
  #description: 'Terminales de pasajeros, líneas de taxi, aeropuertos, agencias de viaje y transportes turísticos.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Transporte created'

#SiteCategory.create(
  #name: 'Recreación y Espacimineto',
  #description: 'Instalaciones deportivas, cines y teatros, centros comerciales y otros espectáculos públicos.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Recreación y Espacimineto created'

#SiteCategory.create(
  #name: 'Parques',
  #description: 'Parques, plazas, zoológicos y reservas de flora y fauna.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Parques created'

#SiteCategory.create(
  #name: 'Alimentos y Bebidas',
  #description: 'Restaurantes, comedores, cafeterías y bares.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Alimentos y Bebidas created'

#SiteCategory.create(
  #name: 'Organismos Públicos',
  #description: 'Alcaldías, policía, destacamentos, bases militares, tribunales, SENIAT, ministerios, CORPOTULIPA, aduanas, consejos comunales, cámaras gremiales.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Organismos Públicos created'

#SiteCategory.create(
  #name: 'Educación',
  #description: 'Universidades, escuelas, liceos, institutos, tecnológicos, preescolares, bibliotecas.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Educación created'

#SiteCategory.create(
  #name: 'Sitios Naturales',
  #description: 'Formaciones de montañas, cerros, sierras, valles, ríos y quebradas, mesetas, salinas, lagunas, caminos pintorescos.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Sitios Naturales created'

#SiteCategory.create(
  #name: 'Museos',
  #description: 'Museos, galerías, lugares históricos, ruinas y lugares arqueológicos',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Museos created'

#SiteCategory.create(
  #name: 'Instituciones Bancarias',
  #description: 'Bancos y cooperativas de crédito.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Instituciones Bancarias created'

#SiteCategory.create(
  #name: 'Licores',
  #description: 'Bodegones y licorerías.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Licores created'

#SiteCategory.create(
  #name: 'Sitios Nocturnos',
  #description: 'Discotecas, pistas de baile, palacios de eventos, night clubs.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Sitios Nocturnos created'

#SiteCategory.create(
  #name: 'Compras',
  #description: 'Línea blanca, lencería, electrónica, souvenirs, amenidades, regalos, amenidades y ocio',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Compras created'

#SiteCategory.create(
  #name: 'Templos',
  #description: 'Iglesias, capillas, congregaciones y manifestaciones religiosas.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Templos created'

#SiteCategory.create(
  #name: 'Viveres',
  #description: 'Supermercados, mercados, bodegones e hiperbodegones.',
  #icon_white: Faker::Avatar.image,
  #icon_color: Faker::Avatar.image
#)
#puts 'Viveres created'

#50.times do
  #Site.create name: Faker::Company.name,
              #address: Faker::Address.street_address,
              #site_category_id: (SiteCategory.first.id..SiteCategory.last.id).to_a.sample,
              #description: Faker::Company.buzzword,
              #img: Faker::Avatar.image,
              #latitude: Faker::Address.latitude,
              #longitude: Faker::Address.longitude,
              #facebook: Faker::Internet.url('facebook.com'),
              #twitter: Faker::Internet.url('twiiter.com'),
              #web: Faker::Internet.url
#end
