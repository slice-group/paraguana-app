class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :name
      t.string :img
      t.string :category
      t.text :address
      t.text :description
      t.string :facebook
      t.string :twitter
      t.string :web
      t.string :latitude
      t.string :longitude

      t.timestamps null: false
    end
  end
end
