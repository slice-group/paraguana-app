class AddIconsToSiteCategory < ActiveRecord::Migration
  def change
    add_column :site_categories, :icon_white, :string
    add_column :site_categories, :icon_color, :string
  end
end
