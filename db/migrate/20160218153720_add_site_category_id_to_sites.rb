class AddSiteCategoryIdToSites < ActiveRecord::Migration
  def change
    add_column :sites, :site_category_id, :integer
  end
end
