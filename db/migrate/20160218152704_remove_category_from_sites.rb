class RemoveCategoryFromSites < ActiveRecord::Migration
  def change
    remove_column :sites, :category, :string
  end
end
