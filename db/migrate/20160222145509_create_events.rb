class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :address
      t.string :img
      t.text :description
      t.string :web
      t.integer :site_id

      t.timestamps null: false
    end
  end
end
