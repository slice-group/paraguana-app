class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :name
      t.text :description
      t.string :img
      t.integer :site_id

      t.timestamps null: false
    end
  end
end
