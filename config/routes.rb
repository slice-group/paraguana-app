Rails.application.routes.draw do

  root to: 'frontend#index'

  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  resources :admin, only: :index

  scope :admin do
    resources :users do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple,
                                  on: :collection,
                                  as: :destroy_multiple
    end
    resources :sites do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple,
                                  on: :collection,
                                  as: :destroy_multiple
      resources :events do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple,
                                    on: :collection,
                                    as: :destroy_multiple
      end
    end
    resources :site_categories do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple,
                                  on: :collection,
                                  as: :destroy_multiple
    end
    resources :home_images do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple,
                                  on: :collection,
                                  as: :destroy_multiple
    end
  end

  namespace :api do
    namespace :v1 do
      devise_for :users

      get '/categories', to: 'categories#index'

      # Api User
      get '/users/:id', to: 'users#user_profile'
      post '/users/:id/edit', to: 'users#edit_profile'

      # Api Sites
      get '/search', to: 'sites#search'
      get '/sites/:id', to: 'sites#site_profile'

      # Eventos
      get '/sites/:site_id/events', to: 'events#index'
      get '/sites/:site_id/events/:id', to: 'events#show'

      scope 'sites/:site_id' do
        resources :comments, only: [:create, :destroy]
        post '/like', to: 'socials#likes'
        post '/check', to: 'socials#checks'
      end
    end
  end

  # Errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all

  # Dashboard
  mount KepplerGaDashboard::Engine, at: '/', as: 'dashboard'
end
