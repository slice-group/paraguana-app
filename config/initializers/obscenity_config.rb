Obscenity.configure do |config|
  config.blacklist  = Rails.root.join('config/locales/obscenity/blacklist.yml')
end